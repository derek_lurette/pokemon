//
//  PokemonAnnotation.swift
//  Pokemon
//
//  Created by Derek Lurette on 2017-11-18.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

import UIKit
import MapKit

class PokemonAnnotation : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var pokemon: Pokemon
    
    init(coordinate: CLLocationCoordinate2D, pokemon: Pokemon) {
        self.coordinate = coordinate
        self.pokemon = pokemon
    }
}
