//
//  ViewController.swift
//  Pokemon
//
//  Created by Derek Lurette on 2017-11-17.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate , MKMapViewDelegate{
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var centerTapped: UIButton!
    var manager = CLLocationManager()
    var updateCount = 0
    var pokemons: [Pokemon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pokemons = getAllPokemon()
        
        manager.delegate = self
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            setup()
        } else {
            manager.requestWhenInUseAuthorization()
        }
    }
    
    func setup() {
        mapView.delegate = self
        mapView.showsUserLocation = true
        manager.startUpdatingLocation()
        
        Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: { (timer) in
            if let coordinate = self.manager.location?.coordinate {
                let pokemon = self.pokemons[Int(arc4random_uniform(UInt32(self.pokemons.count)))]
                let annotation = PokemonAnnotation(coordinate: coordinate, pokemon: pokemon)
                let randomLatitude = (Double(arc4random_uniform(200)) - 100.0) / 50000.0
                let randomLongitude = (Double(arc4random_uniform(200)) - 100.0) / 50000.0
                annotation.coordinate.latitude += randomLatitude
                annotation.coordinate.longitude += randomLongitude
                self.mapView.addAnnotation(annotation)
            }
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if updateCount < 3 {
            let region = MKCoordinateRegionMakeWithDistance(manager.location!.coordinate, 200, 200)
            mapView.setRegion(region, animated: false)
            updateCount += 1
        } else {
            manager.stopUpdatingLocation()
        }
    }
    
    @IBAction func centreTapped(_ sender: Any) {
        if let coordinate = manager.location?.coordinate {
            let region = MKCoordinateRegionMakeWithDistance(coordinate, 200, 200)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
            annotationView.image = UIImage(named: "player")
            
            var frame = annotationView.frame
            frame.size.height = 50
            frame.size.width = 50
            annotationView.frame = frame
            
            return annotationView
        }
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
        let pokemon = (annotation as! PokemonAnnotation).pokemon
        annotationView.image = UIImage(named: pokemon.imageName!)
        
        var frame = annotationView.frame
        frame.size.height = 50
        frame.size.width = 50
        annotationView.frame = frame
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        mapView.deselectAnnotation(view.annotation, animated: true)
        
        if view.annotation is MKUserLocation {
            return
        }
        
        let region = MKCoordinateRegionMakeWithDistance((view.annotation?.coordinate)!, 200, 200)
        mapView.setRegion(region, animated: true)
        let pokemon = (view.annotation as! PokemonAnnotation).pokemon
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (timer) in
            if let coordinate = self.manager.location?.coordinate {
                if MKMapRectContainsPoint(mapView.visibleMapRect, MKMapPointForCoordinate(coordinate)) {
                    pokemon.caught = true
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    mapView.removeAnnotation(view.annotation!)
                    let alertVC = UIAlertController(title: "GOTCHA!", message: "You caught \(pokemon.name!)!", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    let pokedexAction = UIAlertAction(title: "Pokedex", style: .default, handler: {(action) in
                        self.performSegue(withIdentifier: "PokedexSegue", sender: nil)
                    })
                    alertVC.addAction(okAction)
                    alertVC.addAction(pokedexAction)
                    self.present(alertVC, animated: true, completion: nil)
                } else {
                    let alertVC = UIAlertController(title: "OOPS!", message: "You are too far away from \(pokemon.name!), move closer to catch it!", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alertVC.addAction(okAction)
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    
}

